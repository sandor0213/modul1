class PlayersController < ApplicationController
	before_action :find_player, only: [:show, :edit, :update, :destroy]

def index
	@players = Player.all
end

def new
	@player = Player.new
end

def create
 @player = Player.new(player_params)
 if @player.save
 	redirect_to player_path(@player)
 end
end

def show

end


def edit

end

def update
 if @player.update(player_params)
 	redirect_to player_path(@player)
 end
end

def destroy
	@player.destroy
	redirect_to root_path
end

private

def find_player
	@player = Player.find(params[:id])
end

def player_params
	params.require(:player).permit(:nickname, :rank, :charisma, :wisdom)
end



end
